//
// Created by Andrey Prokopenko on 22/04/2020.
// Variant 9
// Task:
// Разработать программу, обеспечивающую разрешение доменного имени (напр.,
// rk6.bmstu.ru) в IP-адрес путем обращения по протоколу DNS (UDP, порт 53) к
// указываемому DNS-серверу. Полнофункциональным прототипом разрабатываемой
// программы может служить утилита nslookup (или dig). Предусмотреть обработку
// двух типов ответа сервера - А и CNAME.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "net/name_resolution.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Usage: ./lab3 example.com <ip of DNS server>\n");
        return EXIT_FAILURE;
    }
    char dns_server_ip[32] = "8.8.8.8";  // default DNS server (google)
    if (argc > 2) {
        strncpy(dns_server_ip, argv[2], strlen(argv[2]));
    }

    char ip[15] = {0};
    if (!resolve_domain(ip, argv[1], dns_server_ip)) {
        fprintf(stderr, "Error while resolving hostname");
        return EXIT_FAILURE;
    }

    printf("\nNeeded ip is %s\n", ip);
    return EXIT_SUCCESS;
}
