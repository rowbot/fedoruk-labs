//
// Created by Andrey Prokopenko on 22/04/2020.
//
#include <arpa/inet.h>
#include <assert.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "net/name_resolution.h"

char *ip_by_hostname(u_char *host, int query_type, char *dns_server_ip) {
    // UDP packet for DNS request
    u_char packet[DNS_MSG_BUFFER] = {0};
    // Placement syntax for filling 4 and 5 byte in request packet
    u_short *id = (u_short *)&packet;
    *id = (u_short)htons(getpid());
    packet[2] = 1;
    u_short *n_questions = (u_short *)&(packet[4]);
    *n_questions = (u_short)htons(1);  // number of questions in request

    // converts host to dns format and writes it to the DNS_HEADER_LEN+ bytes in the packet
    convert_to_dns_name(&(packet[DNS_HEADER_LEN]), host);
    size_t host_shift = strlen((char *)&(packet[DNS_HEADER_LEN])) + 1;

    // Placement syntax for filling question fields in the request which starts just after hostname
    question_sec_t *info_ptr = (question_sec_t *)&packet[DNS_HEADER_LEN + host_shift];
    info_ptr->type = htons(query_type);  // type of the query (A,MX,CNAME,NS...)

    // The CLASS of resource records being requested e.g. Internet, CHAOS etc.
    // 1 is equal the Internet resource record
    // (refer to IANA http://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml)
    info_ptr->class = htons(1);

    // sending the request
    struct sockaddr_in origin = {0};
    origin.sin_family = AF_INET;
    origin.sin_port = htons(DNS_UDP_PORT);
    origin.sin_addr.s_addr = inet_addr(dns_server_ip);
    int s_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sendto(s_fd, (char *)packet, DNS_HEADER_LEN + host_shift + sizeof(question_sec_t), 0,
               (struct sockaddr *)&origin, sizeof(origin)) < 0) {
        fprintf(stderr, "sending the request was failed\n");
        return NULL;
    }

    // receiving the answer
    u_char buf2[DNS_MSG_BUFFER] = {0};
    int ret_val = recvfrom(s_fd, (char *)buf2, DNS_MSG_BUFFER, 0, NULL, NULL);
    if (ret_val < 0) {
        fprintf(stderr, "receiving the answer was failed\n");
        return NULL;
    }

    // move ahead of the dns header and the query field
    u_short *answers_num = (u_short *)&(buf2[6]);
    u_short reverted_answers_num = ntohs(*answers_num);
    u_char *answer_ptr = &buf2[DNS_HEADER_LEN + host_shift + sizeof(question_sec_t)];

    // reading the answers
    resource_rec_t *needed_answer = NULL;
    u_char *data = NULL;
    bool cname_found = false;
    for (int i = 0; i < reverted_answers_num; ++i) {
        int stop = calc_offset(answer_ptr, buf2);
        answer_ptr += stop;
        resource_rec_t *resource = (resource_rec_t *)(answer_ptr);
        answer_ptr += sizeof(resource_rec_t);
        if (ntohs(resource->type) == R_A) {  // if it is an IPv4 address
            data = (u_char *)malloc(ntohs(resource->data_len));
            int data_len = ntohs(resource->data_len);
            strncpy((char *)data, (const char *)answer_ptr, data_len);
            data[data_len] = '\0';
            needed_answer = resource;
        } else if (ntohs(resource->type) == R_CNAME) {
            fprintf(stdout, "Found CNAME record");
            cname_found = true;
            stop = calc_offset(answer_ptr, buf2);
            answer_ptr = answer_ptr + stop;
        }
    }
    if (!cname_found) {
        fprintf(stdout, "Found A record");
    }

    assert(needed_answer != NULL);
    if (ntohs(needed_answer->type) == R_A) {  // if it is an IPv4 address
        long *p = (long *)data;
        struct sockaddr_in a = {0};
        a.sin_addr.s_addr = (*p);
        free(data);
        return inet_ntoa(a.sin_addr);  // convert to human readable format
    }
    if (ntohs(needed_answer->type) == R_CNAME) {  // if it is a CNAME
        return (char *)data;
    }

    fprintf(stderr, "DNS did not return needed records\n");
    free(data);
    return NULL;
}

bool resolve_domain(char *ip, char *domain, char *root) {
    char *result = ip_by_hostname((u_char *)domain, R_A, root);
    if (!result) {  // on error
        ip = NULL;
        return false;
    }
    strncpy((char *)ip, result, strlen(result));
    return true;
}

bool convert_to_dns_name(char *dns_host, char *host) {
    size_t len = strlen(host);
    if (len && host[len - 2] != '.') {
        sprintf(host, "%s.", host);
        len += 2;
    } else {
        return false;
    }
    int count = 0;
    int dns_len = 0;
    for (int i = 0; i < len; ++i) {
        if (host[i] != '.') {
            ++count;
        } else {
            dns_host[dns_len++] = (char)count;
            for (int j = i - count; j < i; ++j) {
                dns_host[dns_len++] = host[j];
            }
            count = 0;
        }
    }
    dns_host[dns_len] = '\0';
    return true;
}

void convert_from_dns(u_char *host, u_char *dns_host) {
    const int kMaxLen = 32;
    int len = strnlen((char *)dns_host, kMaxLen);
    int count = 0;
    for (int i = 0; i < len; ++i) {
        int letters_num = (int)dns_host[i];
        while (count < i + letters_num) {
            host[count] = dns_host[count + 1];
            ++count;
        }
        host[count] = '.';
        ++count;
    }
    host[count] = '\0';
}

int calc_offset(u_char *current_answer, u_char *buffer) {
    bool has_num = false;
    int count = 1;
    while (*current_answer != 0) {
        if (*current_answer >= 127) {  // end of ASCII table
            current_answer = buffer + *(current_answer + 1) - 1;
            has_num = true;  // hostname has an number of letters
        }
        ++current_answer;

        if (!has_num) {
            ++count;  // count number of passed letters for the future
        }
    }
    if (has_num) {
        ++count;  // count number of passed bytes in the answer
    }
    return count;
}