//
// Created by Andrey Prokopenko on 22/04/2020.
//

#ifndef LAB3_NET_NAME_RESOLUTION_H_
#define LAB3_NET_NAME_RESOLUTION_H_

#include <stdbool.h>
#include <sys/types.h>

#define R_A 1               // Ipv4 address
#define R_CNAME 5           // canonical name
#define DNS_UDP_PORT 53     // UDP port for DNS server
#define DNS_MSG_BUFFER 512  // base packet len for dns request/response
                            // https://en.wikipedia.org/wiki/Domain_Name_System#DNS_protocol_transport
#define DNS_HEADER_LEN 12   // dns header len, which can be skipped before useful information

/// DNS question section
/// https://en.wikipedia.org/wiki/Domain_Name_System#Question_section
typedef struct {
    u_short type;
    u_short class;
} question_sec_t;

/// Fields of the resource record
/// https://en.wikipedia.org/wiki/Domain_Name_System#Resource_records
/// We use attribute packed because the size of this resource record is 10 bytes,
/// but the real size in memory is 12 bytes because of LRU CACHE align
typedef struct __attribute__((packed, aligned(2))) {
    u_short type;
    u_short class;
    uint ttl;
    u_short data_len;
} resource_rec_t;

/// Fills the ip of this domain, using DNS A record (IPv4)
/// \param [out] ip - empty array, which will be fill by ip address from
/// DNS server response. On error it will be NULL
/// \param [in] domain - domain name of host which ip user wanna get
/// \param [in] root - DNS server ip, which contains record about passed domain
/// \return result of operation
bool resolve_domain(char *ip, char *domain, char *root);

/// Performs a DNS query with query_type to DNS server which has dns_server_ip
/// by sending a packet with DNS request for information about host
/// \param [in] host - hostname which ip user wanna get
/// \param [in] query_type - type of IP address which user wanna get (A|NS|MX|SOA|AAAA|...)
/// \param [in] dns_server_ip - DNS server ip, which contains record about
/// passed domain
/// \return if success, pointer to the string with IP of host. On
/// error it returns NULL
char *ip_by_hostname(u_char *host, int query_type, char *dns_server_ip);

/// Converts hostname from dns response (in 3rk65bmstu2ru format) to human
/// readable format (rk6.bmstu.ru)
/// \param [out] host - human readable host (target)
/// \param [in] dns_host - host from dns response
void convert_from_dns(u_char *host, u_char *dns_host);

/// Reads the hostname in a format like 3rk65bmstu2ru and
/// \param [out] current_answer - pointer to the memory with a current dns record
/// \param [in] buffer - pointer to the actual answer buffer
/// \return offset
int calc_offset(u_char *current_answer, u_char *buffer);

/// Convert hostname from ordinary format (like rk6.bmstu.ru) to dns request
/// format (like 3rk65bmstu2ru)
/// \param [out] dns_host - host for dns request (target)
/// \param [in] host - human readable host
/// \return result of operation
bool convert_to_dns_name(char *dns_host, char *host);

#endif  // LAB3_NET_NAME_RESOLUTION_H_
