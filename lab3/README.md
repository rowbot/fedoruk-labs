# Лабораторная работа 3  
Выполнена: Прокопенко Андрей, РК6-61Б  
Вариант 9

## Требования: 
1. cmake 3.2+
2. make 4+
3. gcc 5+

## Запуск  
```bash
$ mkdir build && cd build
$ cmake .. && make -j 4
$ ./lab3 <hostname> [OPTIONAL] <ip address of DNS server>  
```   

