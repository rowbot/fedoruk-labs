//
// Created by Andrey Prokopenko on 03/04/2020.
//

// Task:
// Разработать программу, функционирующую в рамках двух процессов (отец и сын).
// Процесс-отец считывает символы со стандартного ввода посредством
// read(0,&ch,1) и передаёт процессу-сыну двоичные коды считанных символов
// посредством отправки сигналов SIGUSR1 (нулевой бит) и SIGUSR2 (единичный
// бит). Процесс-сын выводит переданные ему символы на стандартный вывод с
// помощью write(1,&ch,1).

// Addition task:
// Продумать реализацию передачи символов кодами азбуки Морзе.

#include <ctype.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const int kBitsInChar = 8; // number of bits in one byte

// number of letters in the English alphabet and digits 0-9
const int kAlphabetSymbolsNum = 36;

// 0bXXXXXXXX - it's a GNU extension
const char kMorseCode[36][3] = {
    // i could use define for 36 number but i don't like it

    // {Morse code, number of bits in the code, character}
    {0b01, 2, 'a'},    {0b0001, 4, 'j'},  {0b111, 3, 's'},   {0b1110, 4, 'b'},
    {0b010, 3, 'k'},   {0b0, 1, 't'},     {0b1010, 4, 'c'},  {0b1101, 4, 'l'},
    {0b011, 3, 'u'},   {0b100, 3, 'd'},   {0b00, 2, 'm'},    {0b0111, 4, 'v'},
    {0b1, 1, 'e'},     {0b10, 2, 'n'},    {0b001, 3, 'w'},   {0b1011, 4, 'f'},
    {0b000, 3, 'o'},   {0b0110, 4, 'x'},  {0b100, 3, 'g'},   {0b1001, 4, 'p'},
    {0b0010, 4, 'y'},  {0b1111, 4, 'h'},  {0b0100, 4, 'q'},  {0b1100, 4, 'z'},
    {0b11, 2, 'i'},    {0b101, 3, 'r'},

    {0b00001, 5, '1'}, {0b11110, 5, '6'}, {0b00011, 5, '2'}, {0b11100, 5, '7'},
    {0b00111, 5, '3'}, {0b11000, 5, '8'}, {0b01111, 5, '4'}, {0b10000, 5, '9'},
    {0b11111, 5, '5'}, {0b00000, 5, '0'},
};

char g_data = 0;   // global variable for collection data
int g_counter = 0; // global variable, current bit in a sending byte

/// Gets position of the symbol in the kMorseCode array.
/// \param {char} symbol - inputted symbol
/// \return {int} - position of the symbol in the kMorseCode array or -1 if an
/// error occurred
int get_morse_symbol(char symbol) {
  // character cast to lowercase
  char std_c = (char)tolower(symbol);
  int i = 0;
  // search of a match
  while (i < kAlphabetSymbolsNum && std_c != kMorseCode[i][2]) {
    ++i;
  }
  // error - symbol not found in the kMorseCode array
  if (i == kAlphabetSymbolsNum) {
    return -1;
  }
  return i;
}

/// Gets position of the symbol in the kMorseCode array by its code and number
/// of bits
/// \param {char} code - symbol Morse code \param {int} sig_num - number
/// of bits in Morse code of the symbol
/// \return - position of the symbol in the kMorseCode array. -1 if an error
/// occurred
int get_symbol_from_morse(char code, int sig_num) {
  int i = 0;
  while (i < kAlphabetSymbolsNum &&
         !(code == kMorseCode[i][0] && sig_num == kMorseCode[i][1])) {
    ++i;
  }
  if (i == kAlphabetSymbolsNum) {
    return -1;
  }
  return i;
}

/// Writes one byte to stdout
/// \param {char} data - symbol
void output_writer(char data) {
  // print result symbol
  write(STDOUT_FILENO, &data, 1);
  // flush stream
  fflush(stdout);
}

/// Drops bit counter and resets data collection variable
void erase_global_vars() {
  g_counter = 0;
  g_data = 0;
}

/// Takes SIGUSR1, SIGUSR2 signals which encode char symbols and decode these to
/// symbol, using global variables g_data and g_counter.
/// \param {int} sig - signal code (it suppose to be 30 (SIGUSR1), 31 (SIGUSR2))
void take_and_decode(int sig) {
  // reinitialization of signal handler
  signal(SIGUSR1, take_and_decode);
  signal(SIGUSR2, take_and_decode);
  signal(SIGURG, take_and_decode);

  // signal handling and collect store bit to the data variable
  switch (sig) {
  case SIGUSR1: { // 0 bit
    g_data |= 0 << g_counter;
    break;
  }

  case SIGUSR2: { // 1 bit
    g_data |= 1 << g_counter;
    break;
  }

  case SIGURG: { // end of character

    int sym_num = get_symbol_from_morse(g_data, g_counter);
    kill(getppid(), SIGCONT);
    if (sym_num < 0) {
      fprintf(stderr, "Unknown morse code: [%d], [%d]\n", g_data, g_counter);
      fflush(stderr);
      erase_global_vars();
      return;
    }
    output_writer(kMorseCode[sym_num][2]);
    erase_global_vars();
    return;
  }
  // it's not necessary. just in case
  default:
    --g_counter;
    fprintf(stderr, "Unknown signal!\n");
    fflush(stderr);
  }
  ++g_counter;
  // send continue signal to parent process
  kill(getppid(), SIGCONT);
}

/// Function handles continue signal from another process
/// \param _ - function has this parameter due to signal handler function
/// signature (describe in signal.h as /* Type of a signal handler.  */
/// typedef void (*__sighandler_t) (int)) )
void handle_resume(int _) { signal(SIGCONT, handle_resume); }

/// This function encode char from argument by bits and send it to
/// another process, using signals SIGUSR1 ans SIGUSR2
/// \param {pid_t} pid - process identifier
/// \param {char} symbol - symbol which have to be send
void encode_and_send(pid_t pid, char symbol) {
  // loop from first bit to last bit in byte
  for (int i = 0; i < kBitsInChar; ++i) {
    // if i bit equals 1 then send SIGUSR1 signal, else send SIGUSR2 signal
    if (symbol & (1 << i)) {
      kill(pid, SIGUSR2);
    } else {
      kill(pid, SIGUSR1);
    }
    pause();
  }
}

/// This function is featured version of encode_and_send.
/// It either encodes char from argument by bits and send it to
/// another process but uses for this operation kMorseCode array and signals
/// SIGUSR1 ans SIGUSR2. When it forward all bits to another process it send
/// signal SIGURG as end of character
/// \param {pid_t} pid - process identifier
/// \param {char} sym_num - position of
/// symbol in the kMorseArray which have to be send
void encode_and_send2(pid_t pid, int sym_num) {
  // if wrong symbol number we prevent out of range error
  if (sym_num > kAlphabetSymbolsNum) {
    return;
  }

  for (int i = 0; i < kMorseCode[sym_num][1]; ++i) {
    // if i bit equals 1 then send SIGUSR1 signal, else send SIGUSR2 signal
    if (kMorseCode[sym_num][0] & (1 << i)) {
      kill(pid, SIGUSR2);
    } else {
      kill(pid, SIGUSR1);
    }
    pause();
  }
  kill(pid, SIGURG);
  pause();
}

/// Input reader function. Reads one byte from stdin, encode it and send to
/// process which pid it has
/// \param {pid_t} pid - process identifier
void input_reader(pid_t pid) {
  char data = 0;

  // While not an end of file read data from stdin
  while (data != EOF) {
    read(STDIN_FILENO, &data, 1);
    // check if enter was inputted. if yes - do nothing
    if (data != '\n') {
      int sym_num = get_morse_symbol(data);
      // check on errors
      if (sym_num >= 0) {
        // encode and send one byte to another process with this pid
        encode_and_send2(pid, sym_num);
      } else {
        fprintf(stderr, "Unknown symbol: [%c]\n", data);
        fflush(stderr);
      }
    }
  }
}

/// Main function of program. Initializes signal handlers, create two process
/// and initializes event loops
/// \param {int} argc - command line argument counter
/// \param {char**} argv - vector of arguments from command line
/// \return {int} - status code
int main(int argc, char **argv) {
  // create child process
  pid_t pid = fork();
  if (pid == -1) { // error occurred
    perror("fork error");
    return EXIT_FAILURE;
  }
  if (pid == 0) { // child process
    // initialize signal handlers
    signal(SIGUSR1, take_and_decode);
    signal(SIGUSR2, take_and_decode);
    signal(SIGURG, take_and_decode);

    // wait for signals
    while (true);

    exit(EXIT_SUCCESS);
  } else { // parent process
    // initialize signal handler
    signal(SIGCONT, handle_resume);

    fprintf(stdout, "Child pid is %d\n", pid);
    fflush(stdout);
    // start read data from stdin and send it to another process
    input_reader(pid);
  }

  return EXIT_SUCCESS;
}
