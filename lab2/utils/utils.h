//
// Created by Andrey Prokopenko on 12/04/2020.
//

#ifndef LAB2_LAB2_INCLUDE_UTILS_H_
#define LAB2_LAB2_INCLUDE_UTILS_H_
#define DEFAULT_N_THREAD 4
#define OPT_KEYS ".t:s:n:"

typedef enum status {
    NOT_FOUND = -3,
    UNKNOWN_FLAG,
    ERROR,
    OK,
    TIME,
    THREAD,
    IS_FILE,
} status_t;

typedef struct {
    unsigned int n_thread;
    unsigned modeling_time;
    unsigned n_grid_nodes;
} settings_t;

/// Get settings from command line
/// \param settings
/// \param argc
/// \param argv
/// \return
status_t get_settings(settings_t *settings, int argc, char **argv);

/// Show details about program
void usage_details();

/// Convert char* argument to int
/// \param arg
/// \return
int convert_to_int(char *arg);

/// Plot graphics (two lines) using n_points as X and points_y1, points_y2 as Y1 and Y2.
/// \param n_commands
/// \param script
/// \param n_points
/// \param points_x
/// \param points_y1
void plot_graphic(int n_commands, char** script, int n_points, double *points_x,
                  double* points_y1, double* points_y2);

/// Plot graphic in 3D using n_points as X and points_y, points_z as Y1 and Y2.
/// \param n_commands
/// \param script
/// \param n_points
/// \param points_x
/// \param points_y
void plot_3d_graphic(int n_commands, char** script, int n_points, double* points_x,
                     int n_z_points, double** points_y, double* points_z);


/// Create animation using n_points and points_y, and points_z as timer.
/// \param n_commands
/// \param script
/// \param n_points
/// \param points_x
/// \param points_y
void draw_animation(int n_commands, char** script, int n_points, double* points_x,
                    int n_z_points, double** points_y);

#endif  // LAB2_LAB2_INCLUDE_UTILS_H_

