//
// Created by Andrey Prokopenko on 13/04/2020.
//

#include "fnd_consist.h"
#include <stddef.h>
#include <stdlib.h>
#include <sys/time.h>
#include "common/common.h"

bool create_grid(grid_t *grid, unsigned int n_size, int time) {
    grid->gplt = fopen("/tmp/lab2.dat", "w+");
    if (!grid->gplt) {
        return false;
    }
    grid->prev = (double *)calloc((size_t)n_size, sizeof(double));
    if (!grid->prev) {
        fclose(grid->gplt);
        return false;
    }
    grid->current = (double *)calloc((size_t)n_size, sizeof(double));
    if (!grid->current) {
        free(grid->prev);
        fclose(grid->gplt);
        return false;
    }
    grid->old = (double *)calloc((size_t)n_size, sizeof(double));
    if (!grid->old) {
        free(grid->prev);
        free(grid->current);
        fclose(grid->gplt);
        return false;
    }

//    grid->item = (double **)calloc(time, sizeof(double *));
//    if (!grid->item) {
//        return false;
//    }
//    for (int i = 0; i < time; ++i) {
//        grid->item[i] = (double *)calloc((size_t)n_size, sizeof(double));
//        if (!grid->item[i]) {
//            for (int j = 0; j < i; ++j) {
//                free(grid->item[j]);
//            }
//            return false;
//        }
//    }
    grid->size2 = n_size;
    grid->size1 = time;
    grid->dt = DT; //((double)time) / L_LIMIT;
    grid->dx = X_LIMIT / n_size;
    return true;
}

bool init_start_conditions(grid_t *grid, condition_t *conditions, int n_cond) {
    if (conditions != NULL && n_cond > 0) {
        for (int i = 0; i < n_cond; ++i) {
            if (grid->size2 < conditions[i].i) {
                return false;
            }
            grid->prev[conditions[i].i] = conditions[i].condition;
            grid->old[conditions[i].i] = conditions[i].condition;
        }
    }
    return true;
}

bool condition_approximation(grid_t *grid, derivative_cond_t_func d_cond, double a_k) {
    if (d_cond != NULL && grid->size1 > 1) {
        for (int i = 0; i < grid->size2; ++i) {
            grid->prev[i] =
                grid->dt * (d_cond(grid->old[i]) +
                            grid->dt / 2 *
                                (a_k * a_k *
                                 (grid->old[i + 1] - 2 * grid->old[i] +
                                  grid->old[i - 1]))) + grid->old[i];
        }
        return true;
    }
    return false;
}

void solve_wave_equation(grid_t *res, double *time_ptr, external_exposure_t_func func,
                         double a_k) {
    struct timeval tv1;
    gettimeofday(&tv1, NULL);
    for (int k = 1; k < (int)res->size1 - 1; ++k) {
        for (int i = 1; i < res->size2; ++i) {
            res->current[i] =
                res->dt * res->dt * a_k * a_k / (res->dx * res->dx) *
                    (res->prev[i + 1] - 2 * res->prev[i] + res->prev[i - 1]) +
                2 * res->prev[i] - res->old[i] +
                res->dt * res->dt * func(i * res->dx, k * res->dt);
        }
//        for (int i = 0; i < res->size2; ++i) {
//            fprintf(res->gplt, "%lf %lf ", res->dx * i, res->current[i]);
//        }
//        fprintf(res->gplt, "\n");
        double *tmp = res->old;
        res->old = res->prev;
        res->prev = res->current;
        res->current = tmp;
    }
    struct timeval tv2;
    gettimeofday(&tv2, NULL);
    if (time_ptr) {
        *time_ptr = (double)(tv2.tv_usec - tv1.tv_usec) +
                    (double)(tv2.tv_sec - tv1.tv_sec) * 100000;
    }
}
