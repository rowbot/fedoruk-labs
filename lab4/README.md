# Лабораторная работа 4  
Выполнена: Прокопенко Андрей, РК6-61Б  
Вариант 8

## Требования: 
1. cmake 3.2+
2. make 4+
3. gcc 5+
4. gnuplot 5.2+
5. mpich 3.2+

## Запуск  
```shell script
mkdir build && cd build
cmake .. -DCMAKE_C_COMPILER=mpicc && make -j 4
mpirun -np <number of processes> ./lab4 -s <time of calculus> -n <number of grid nodes>  
```  
Для справки:  
```shell script
$ ./lab4 -h  
```  

Пример работы:   
```shell script
$ mpirun -n 4 ./lab4 -s 500 -n 104
Total time: 4716.000000 ms
```
По заврешению работы программы:  
```shell script
$ cd .. 
$ rm -r build
```
