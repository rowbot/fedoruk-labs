#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "euler/euler.h"
#include "euler/utils.h"
#define BENCHMARK

int main(int argc, char **argv) {
  settings_t settings = {0};
  // gets user settings from the command line
  if (get_settings(&settings, argc, argv) != OK) {
    return EXIT_FAILURE;
  }
  grid_t grid = {0};

  if (MPI_Init(&argc, &argv) != MPI_SUCCESS) {
    return EXIT_FAILURE;
  }

  int size = 0;
  // gets number of all processes
  if (MPI_Comm_size(MPI_COMM_WORLD, &size) != MPI_SUCCESS) {
    MPI_Finalize();
    return EXIT_FAILURE;
  }

  int rank = 0;
  // gets number of current process
  if (MPI_Comm_rank(MPI_COMM_WORLD, &rank) != MPI_SUCCESS) {
    MPI_Finalize();
    return EXIT_FAILURE;
  }

  /// Creates grid initialization
  if (!grid_init(&grid, &settings, size, rank)) {
    MPI_Abort(MPI_COMM_WORLD, 500);
    exit(EXIT_FAILURE);
  }

  struct timeval tv1 = {};
  if (!rank) { // if it is a base process we store time when calculations are
               // started
    gettimeofday(&tv1, NULL);
  }

  // solve the equation
  if (!solve(rank, size, &grid)) {
    MPI_Abort(MPI_COMM_WORLD, 500);
    destroy_grid(&grid, rank);
    exit(EXIT_FAILURE);
  }

  if (!rank) { // if it is our base process we store all
               // data about runtime and print it for user
    struct timeval tv2 = {};
    gettimeofday(&tv2, NULL);
    fprintf(stdout, "Total time: %lf ms\n",
            (double)(tv2.tv_usec - tv1.tv_usec) / 1000 +
                (double)(tv2.tv_sec - tv1.tv_sec) * 1000);

#ifndef BENCHMARK
    /// Create pipeline with gnuplot. Opens an interface that one can use to
    /// send commands as if they were typing into the gnuplot command line. "The
    /// -persistent" keeps the plot open even after your C program terminates.
    FILE *gnuplot = popen("gnuplot -persist", "w");
    fprintf(gnuplot, "set xrange[0:%d]\n", settings.n_grid_nodes - 1);
    fprintf(gnuplot, "do for [i=0:%d]{\n",
            (int)(grid.size1 * 0.1 / CONST_DT) - 3);
    fprintf(
        gnuplot,
        "plot '/tmp/plot_script.gp' index i using 1:2  with lines  title 'U'\n"
        "pause 0.1 }\n"
        "pause -1\n");
    fflush(gnuplot);
    pclose(gnuplot);
#endif
  }

  // frees memory and descriptors
  destroy_grid(&grid, rank);
  MPI_Finalize();
  exit(EXIT_SUCCESS);
}