//
// Created by Andrey Prokopenko on 18/05/2020.
//

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "euler.h"


bool solve(int rank, int size, grid_t *grid) {
  int n_proc_next = (rank == size - 1) ? MPI_PROC_NULL : rank + 1;
  int n_proc_prev = (rank == 0) ? MPI_PROC_NULL : rank - 1;

  int j = 0;
  double dt = CONST_DT;
  double prev = 0;
  double final = 0;
  MPI_Status status = {0};
  while (dt < grid->size1) {
    grid->layer[0] = grid->prev_layer[0] -
                     ((2 * grid->prev_layer[0]) / CONST_R) * (dt / CONST_C);

    if (!rank) {
      grid->layer[0] = 200;
    } else {
      grid->layer[0] +=
          ((prev + grid->prev_layer[1]) / CONST_R) * (dt / CONST_C);
    }

    for (int i = 1; i + 1 < grid->window; ++i) {
      grid->layer[i] =
          ((grid->prev_layer[i - 1] - grid->prev_layer[i]) / CONST_R -
           (grid->prev_layer[i] - grid->prev_layer[i + 1]) / CONST_R) *
              (dt / CONST_C) +
          grid->prev_layer[i];
    }

    int idx = grid->window - 1;
    grid->layer[idx] = grid->prev_layer[idx] -
                       ((2 * grid->prev_layer[idx]) / CONST_R) * (dt / CONST_C);

    if (rank == size - 1) {
      grid->layer[idx] = (dt / grid->size1) * CONST_I * CONST_R;
    } else {
      grid->layer[idx] +=
          ((grid->prev_layer[idx - 1] + final) / CONST_R) * (dt / CONST_C);
    }

    if (rank != size - 1) {
      MPI_Sendrecv(&(grid->layer[grid->window - 1]), 1, MPI_DOUBLE, n_proc_next,
                   2, &final, 1, MPI_DOUBLE, n_proc_next, 2, MPI_COMM_WORLD,
                   &status);
    }

    if (rank != 0) {
      MPI_Sendrecv(&(grid->layer[0]), 1, MPI_DOUBLE, n_proc_prev, 2, &prev, 1,
                   MPI_DOUBLE, n_proc_prev, 2, MPI_COMM_WORLD, &status);
    }

    MPI_Gather((void *)grid->layer, grid->window, MPI_DOUBLE, (void *)grid->out,
               grid->window, MPI_DOUBLE, 0, MPI_COMM_WORLD);

#ifndef BENCHMARK
    if (!rank && (j % 10 == 0)) {
      // Write input data to /tmp/plot_script.gp file for future use
      for (int k = 0; k < grid->size2; ++k) {
        fprintf(grid->plt, "%d %.10f\n", k, grid->out[k]);
      }
      fprintf(grid->plt, "\n\n");
    }
#endif
    memcpy(grid->prev_layer, grid->layer, grid->window * sizeof(double));
    dt += CONST_DT;
    ++j;
  }
  return true;
}

bool grid_init(grid_t *grid, settings_t *settings, int size, int rank) {
  if (!rank) { // if it is base process
#ifndef BENCHMARK
    grid->plt = fopen("/tmp/plot_script.gp", "w+");
    if (grid->plt == NULL) {
      return false;
    }
#endif
    grid->out = (double *)malloc(sizeof(double) * settings->n_grid_nodes);
    if (grid->out == NULL) {
      fclose(grid->plt);
      return false;
    }
  }

  grid->size1 = (double)settings->modeling_time * CONST_DT;
  grid->window = (int)(settings->n_grid_nodes / size);
  grid->prev_layer = (double *)calloc(grid->window, sizeof(double));
  if (!grid->prev_layer) {
    fclose(grid->plt);
    free(grid->out);
    return false;
  }

  grid->layer = (double *)calloc(grid->window, sizeof(double));
  if (!grid->layer) {
    fclose(grid->plt);
    free(grid->out);
    free(grid->prev_layer);
    return false;
  }
  grid->size2 = settings->n_grid_nodes;
  return true;
}

void destroy_grid(grid_t *grid, int rank) {
  free(grid->prev_layer);
  free(grid->layer);
  if (!rank) {
    free(grid->out);
#ifndef BENCHMARK
    fclose(grid->plt);
#endif
  }
}
