//
// Created by starman on 29/05/2020.
//

#ifndef LAB4_EULER_UTILS_H_
#define LAB4_EULER_UTILS_H_

#include <stddef.h>
#define DEFAULT_N_THREAD 4
#define OPT_KEYS ".s:n:"
#define __STRINGIFY(x) #x
#define STRINGIFY(x) __STRINGIFY(x)
#define LINE_1 "do for [i=1:"
#define LINE_2                                                                 \
  "] { plot '/tmp/data.tmp' u 2*i-1:2*i w l title 'f(x,t)'\npause 0.5 }"
#define SCRIPT_LINE(x) LINE_1 STRINGIFY(x) LINE_2

typedef enum status {
  NOT_FOUND = -3,
  UNKNOWN_FLAG,
  ERROR,
  OK,
  TIME,
  THREAD,
  IS_FILE,
} status_t;

typedef struct {
  unsigned modeling_time;
  unsigned n_grid_nodes;
} settings_t;

typedef struct {
  size_t c_num;
  char **commands;
} script_t;

/// Get settings from command line
/// \param settings
/// \param argc
/// \param argv
/// \return
status_t get_settings(settings_t *settings, int argc, char **argv);

/// Show details about program
void usage_details();

/// Convert char* argument to int
/// \param arg
/// \return
int convert_to_int(char *arg);

#endif // LAB4_EULER_UTILS_H_
