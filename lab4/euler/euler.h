//
// Created by Andrey Prokopenko on 18/05/2020.
//

#ifndef LAB4_EULER_EULER_H_
#define LAB4_EULER_EULER_H_
#include "utils.h"
#include <stdbool.h>
#include <stdio.h>
#define CONST_R 200
#define CONST_C 0.000002
#define CONST_DT 1e-8
#define CONST_I 10

typedef struct {
  unsigned int window;
  unsigned int size2;
  double size1;
  double *prev_layer;
  double *layer;
  double *out;
  FILE *plt;
} grid_t;

/// Solves equation using MPI and writes the results to the file
/// which id contains in the grid structure.
/// \param [in] rank - rank of current proccess
/// \param [in] size - total number of processes
/// \param [in,out]grid - initialized by start conditions grid
/// \return status of operation
bool solve(int rank, int size, grid_t *grid);

/// Allocates memory for grid elements by settings and initialize several fields
/// there
/// \param [out] grid - empty object that might be initialized
/// \param [in] settings - user input from the command line
/// \param [in] size - total number of processes
/// \param [in] rank - rank of current proccess
/// \return result of operation
bool grid_init(grid_t *grid, settings_t *settings, int size, int rank);

/// Frees allocated to grid memory
/// \param [in,out] grid1
void destroy_grid(grid_t *grid, int rank);

#endif // LAB4_EULER_EULER_H_
