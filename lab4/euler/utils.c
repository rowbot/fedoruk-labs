//
// Created by Andrey Prokopenko on 25/05/2020.
//

#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "utils.h"

int convert_to_int(char *arg) {
  char *end = NULL;
  int n_thread = (int)strtol(arg, &end, 10);
  // Check for various possible errors
  if ((errno == ERANGE && (n_thread == INT_MAX || n_thread == INT_MIN)) ||
      (errno != 0 && n_thread == 0)) {
    return -1;
  }

  if (end == optarg) {
    fprintf(stderr, "No digits were found\n");
    return -1;
  }
  return n_thread;
}

void usage_details() {
  printf("Usage: lab2 [OPTION] [OPTION] [OPTION]\n"
         "Example: lab2 -s 10 -n 200\n"
         "------- Listing options -------\n"
         "-s seconds        Set program executable time\n"
         "-n nodes          Nodes of calculus grid. IMPORTANT! [nodes mod 8] "
         "HAVE TO be "
         "equal 0\n");
}

status_t get_settings(settings_t *settings, int argc, char **argv) {
  settings_t default_settings = {};
  status_t status = OK;

  if (argc < 5) {
    status = ERROR;
  }

  int opt = getopt(argc, argv, OPT_KEYS);
  while (opt != -1 && status != ERROR) {
    if (!optarg) {
      status = ERROR;
      continue;
    }

    switch (opt) {
    case 'n': {
      default_settings.n_grid_nodes = convert_to_int(optarg);
      if (default_settings.n_grid_nodes == INT_MAX ||
          default_settings.n_grid_nodes % 8 != 0) {
        status = ERROR;
      }
      break;
    }

    case 's': {
      default_settings.modeling_time = convert_to_int(optarg);
      if (default_settings.modeling_time == INT_MAX) {
        status = ERROR;
      }
      break;
    }

    case 'h':
    case '?': {
      status = UNKNOWN_FLAG;
      usage_details();
      break;
    }

    default: {
      status = UNKNOWN_FLAG;
      //      usage_details();
      break;
    }
    }
    opt = getopt(argc, argv, OPT_KEYS);
  }

  if (status == ERROR) {
    usage_details();
  }
  *settings = default_settings;
  return status;
}

